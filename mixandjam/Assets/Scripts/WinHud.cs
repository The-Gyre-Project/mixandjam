﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WinHud : MonoBehaviour
{

    public Text scoreCount;
    //public Text winText;
    public int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        scoreCount = GetComponent<Text>();
        SetCountText();
       // winText.text = "";

    }

    private void FixedUpdate()
    {
        SetCountText();
    }

    void SetCountText()
    {
        scoreCount.text = "Score Count: " + count.ToString();
        if (count >= 12)
        {
         //   winText.text = "You Win!";
        }
    }
}