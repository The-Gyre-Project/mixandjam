﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{
    private Transform tf;
    private Rigidbody rb;
    private Transform playerTF;
    private Vector3 cameraOffsetFromPlayer;
    private float yDistance = 25;

    private Vector3 targetPosition;

    void Start()
    {
       
        playerTF = GameObject.Find("Player").GetComponent<Transform>();
        cameraOffsetFromPlayer = tf.position - playerTF.position;
    }

    private void Awake()
    {
        tf = GetComponent<Transform>();
    }

    void Update()
    {
        targetPosition = playerTF.position - playerTF.forward * cameraOffsetFromPlayer.magnitude;
        targetPosition.y = playerTF.position.y + yDistance;
        transform.position = targetPosition;

        transform.LookAt(playerTF.position);
        
    }
}