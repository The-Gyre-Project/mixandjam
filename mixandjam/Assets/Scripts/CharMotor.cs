﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animation_Player;
using UnityEngine.SceneManagement;

public class CharMotor : MonoBehaviour
{
    public Transform tf;
    public Rigidbody rb;
    public AnimationPlayer ap;

    private GameObject cam;
    private Transform camTf;
    private ScoreHud scoreHud;

    private float speed = 5;
    public float maxYSpeed = 0;
    private float launchStrenght = 2;

    [SerializeField]
    private bool isJumping = false;
    private bool firstJump = false;
    private bool doingTrick = false;
    private float trickTime = 0;
    private Quaternion initialRot;
    [SerializeField]
    private bool hasBounced = false;
    [SerializeField]
    private bool slamming = false;
    [SerializeField]
    private float jumpStrenght = 10;
    [SerializeField]
    private float floorBounce = 0.05f;
    [SerializeField]
    private float crowdBounce = 0.9f;

    private float impulseLenghtMax = 0;
    private float impulseLenght = 0;

    private Transform peanutTf;

    public float ySpeed = 0;

    GameObject col = null;

    private float lostTimer = 3;
    private float lostTimerMax = 3;

    void Awake()
    {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        ap = GetComponentInChildren<AnimationPlayer>();
        ap.RegisterAnimationEventListener("AnimTransition", AnimTransition);

        cam = GameObject.Find("Main Camera");
        camTf = cam.GetComponent<Transform>();

        peanutTf = this.transform.Find("mrpeanutneutral").GetComponent<Transform>();
        scoreHud = GameObject.Find("Canvas").GetComponentInChildren<ScoreHud>();
        
        ySpeed = rb.velocity.y; 
    }

    public void AnimTransition()
    {
        AnimationPlayerState state = ap.GetPlayingState();
        if (state.Name == "Armature|impulse")
        {
            Quaternion q = tf.rotation;
            tf.rotation = Quaternion.identity;
            peanutTf.position = new Vector3(peanutTf.position.x, peanutTf.position.y + 0.8f, peanutTf.position.z + 2);
            tf.rotation = q;
            ap.Play("Armature|surfing");
            impulseLenghtMax = ap.GetState("Armature|surfing").Duration;
            
            rb.AddForce(new Vector3(0, jumpStrenght, 0), ForceMode.Impulse);
            isJumping = true;
        }
        if (state.Name == "Armature|surfing")
        {
            if (!firstJump)
                firstJump = true;
        }
    }

        void APUpdate()
    {
        AnimationPlayerState state = ap.GetPlayingState();
        if (state.Name == "Armature|neutral3")
        {
            if (!isJumping && Input.GetButtonDown("Jump"))
            {
                AnimationPlayerState s1 = ap.GetState("Armature|impulse");
                ap.Play("Armature|impulse");
            }
        }
        if (state.Name == "Armature|impulse")
        {
           // Debug.Log("IMPULSE  " + peanutTf.rotation.eulerAngles);
        }
        if (state.Name == "Armature|surfing")
        {
            if (!firstJump) { 
                //why suddenly 1/4th of 90?!
                float delta = (270f / impulseLenghtMax) * Time.deltaTime;
                peanutTf.rotation = Quaternion.AngleAxis(delta, peanutTf.forward) * peanutTf.rotation;
            }

            if (!isJumping && Input.GetButtonDown("Jump"))
            {
                // rb.AddForce(new Vector3(0, jumpStrenght, 0), ForceMode.Impulse);
                rb.position = new Vector3(rb.position.x, rb.position.y + 0.2f, rb.position.z);
                rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y+jumpStrenght, rb.velocity.z);
                isJumping = true;
            }

            
            if (isJumping && Input.GetKeyDown(KeyCode.LeftShift) && !doingTrick)
            {
                doingTrick = true;
                trickTime = impulseLenghtMax;
                initialRot = rb.rotation;
                //ap.Play("Trick3");
                //ap.QueueStateChange("Armature|surfing");

            }
            if (doingTrick)
            {
                if (trickTime>0)
                {
                    float delta = (360f / impulseLenghtMax) * Time.deltaTime;
                    peanutTf.rotation = Quaternion.AngleAxis(delta, peanutTf.up) * peanutTf.rotation;
                }
                else
                {
                    rb.rotation = initialRot;
                    doingTrick = false;
                    scoreHud.count += 5;
                }
                trickTime -= Time.deltaTime;
            }
            
        }

        ySpeed = rb.velocity.y;
    }

    private float timer = 1f;
    void FixedUpdate()
    {
        timer -= Time.deltaTime;
        if (isJumping)
            if (timer<=0)
                scoreHud.count += 1;
        if (timer <= 0)
            timer = 1f;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("StefScene");
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        Debug.Log(            tf.forward);
        Vector3 desiredMoveDir = new Vector3(0.0f, 0.0f, Input.GetAxisRaw("Vertical"));
        Vector3 distanceMoved = MakeDirectionCamRelative(desiredMoveDir).normalized * speed * Time.deltaTime;

        APUpdate();

        Vector3 target_pos = rb.position + distanceMoved;

        float yRot = tf.rotation.y + Input.GetAxisRaw("Horizontal") * Time.deltaTime * 50;
        tf.Rotate(0, Input.GetAxisRaw("Horizontal") * 60 * Time.deltaTime, 0);

        if (Input.GetKeyDown(KeyCode.W))
            rb.AddForce(tf.forward * launchStrenght, ForceMode.Impulse);


        if (rb.velocity.y < 0)
            Physics.gravity = new Vector3(0, -20f, 0);
        else
        {
            Physics.gravity = new Vector3(0, -9.8f, 0);
            //if going up is not slamming
            slamming = false;
        }

        if (isJumping && Input.GetAxisRaw("Vertical") < 0)
        {
            // if jumping and down, slam (potentially negate slam above)
            slamming = true;
            Physics.gravity = new Vector3(0, -50f, 0);
            //rb.AddForce(Vector3.down * 50);
            //Debug.Log("SLAM");
        }
        else if (Input.GetAxisRaw("Vertical") >= 0)
        {
            //otherwise you're not slamming
            slamming = false;
        }

        if (!isJumping && Input.GetButtonDown("Jump"))
        {
            //  rb.AddForce(new Vector3(0, jumpStrenght, 0), ForceMode.Impulse);
            //  isJumping = true;
            /*
            if (!firstJump)
            {
                //does this get the model for sure? maybe if model is first child
                Transform t = this.transform.Find("mrpeanutneutral").GetComponent<Transform>();
                t.Rotate(new Vector3(t.rotation.x, t.rotation.y, -90));
                t.position = new Vector3(t.position.x, t.position.y+ 0.7f, t.position.z+2);
                firstJump = true;
            }
            */
        }

        if (rb.velocity.y <= 0)
            hasBounced = false;


        if (rb.velocity.y < 0)
        {

            if (rb.velocity.y < maxYSpeed)
                maxYSpeed = -rb.velocity.y;
        }
        else
            maxYSpeed = 0;

        //Debug.DrawRay(transform.position, -Vector3.up, Color.red);
        RaycastHit hitInfo;
        Vector3 p = new Vector3(tf.position.x, tf.position.y, tf.position.z);
        p.y += 0.2f;

        //if (Physics.CapsuleCast(p, p2, charContr.radius, transform.forward, out hit, 10))


        bool m_HitDetect = Physics.BoxCast(p, new Vector3(4, 0.1f, 4.6f), -Vector3.up, out hitInfo, transform.rotation, Mathf.Infinity);
        if (Input.GetButton("Fire1") && m_HitDetect)//Physics.Raycast(p, -Vector3.up, out hitInfo, Mathf.Infinity))
        {
            
            Debug.Log(hitInfo.transform.gameObject.name);
            Debug.DrawLine(transform.position, hitInfo.point, Color.red);
            if (hitInfo.transform.gameObject.name == "Hud")
                if (Input.GetButtonDown("Fire1") && Physics.Raycast(hitInfo.transform.position, -Vector3.up, out hitInfo, Mathf.Infinity))
                    Debug.Log("               "+hitInfo.transform.gameObject.name);
        }

        Debug.Log(rb.position.y);
        if (rb.position.y<=0.1)
        {
            if (lostTimer <= 0)
            {
                scoreHud.Win();
            }
            lostTimer -= Time.deltaTime;
        }
    }

    public void ReceiveCollision(GameObject hit)
    {
        col = hit;
    }

    public void OnCollisionEnter(Collision hit)
    //public void ReceiveCollision(Collision hit)
    {
        if (col != null)
            Debug.Log(hit.gameObject.tag + "     col  " + col.name);
        //Debug.Log("vel       " + rb.velocity);
        Physics.gravity = new Vector3(0, -9.8f, 0);

        if (hit.gameObject.tag == "Hud" && col != null)
        {
            slamming = false;

            //Debug.Log(hit.gameObject.tag + "     col  " + col.name);
            if (col.tag == "Environment")
            {
                Physics.gravity = new Vector3(0, -9.8f, 0);
                isJumping = false;
                if (doingTrick)
                {
                    doingTrick = false;
                    rb.rotation = initialRot;
                }
                float bounce = floorBounce * maxYSpeed;
                //rb.velocity = new Vector3(0, bounce, 0);
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                rb.AddForce(new Vector3(0, bounce, 0), ForceMode.Impulse);
                hasBounced = true;
                //Debug.Log("new vel       " + rb.velocity);
            }

            if (col.tag == "Crowd")
            {
                Physics.gravity = new Vector3(0, -9.8f, 0);
                //shouldn't be here but is for testing
                isJumping = false;
                if (doingTrick)
                {
                    doingTrick = false;
                    rb.rotation = initialRot;
                }
                
                float bounce = crowdBounce * maxYSpeed;
                // Debug.Log("maxYSpeed     " + maxYSpeed);
                // Debug.Log("GetBounciness     " + hit.gameObject.GetComponent<Crowd>().GetBounciness());
                if (bounce <= 0.05)
                    bounce = 0.05f;
                if (!hasBounced)
                {
                    rb.position = new Vector3(rb.position.x, rb.position.y+1f, rb.position.z);
                    rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.y);
                    rb.AddForce(new Vector3(0, bounce, 0), ForceMode.Impulse);
                    hasBounced = true;
                }
                Debug.Log("new vel       " + rb.velocity + "bounce      " + bounce);
                scoreHud.count += 5;
            }
            else if (hit.gameObject.tag != "Hud")
            {
                //Debug.Log(hit.gameObject.tag + "     col  " + col.name);
                //This might break things, but at least now I only bounce where the HUD is.
                Physics.IgnoreCollision(GetComponent<Collider>(), hit.collider);
            }
        }
    }

    public void OnTriggerEnter(Collider hit)
    {
        Debug.Log("OnTriggerEnter "+hit.gameObject.tag);
        if (hit.gameObject.tag == "Collectible")
        {
            Destroy(hit.gameObject);
            rb.AddForce(tf.forward * 5, ForceMode.Impulse);
            rb.velocity = new Vector3(rb.velocity.x, (rb.velocity.y >= 0) ? rb.velocity.y : 0, rb.velocity.z);
            rb.AddForce(tf.up * 4, ForceMode.Impulse);
        }
    }



    public Vector3 MakeDirectionCamRelative(Vector3 direction)
    {
        Vector3 camFwdRelative = camTf.forward * direction.z;
        Vector3 camRgtRelative = camTf.right * direction.x;

        Vector3 camRelativeMove = camFwdRelative + camRgtRelative;
        camRelativeMove.y = 0.0f;

        return camRelativeMove;
    }
}
