﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]
public class PlayerHudFollower : MonoBehaviour
{
    private Transform tf;

    private Transform tfPlayer;
    private CharMotor cmPlayer;

    private void Awake()
    {
        tfPlayer = GameObject.Find("Player").GetComponent<Transform>();
        cmPlayer = GameObject.Find("Player").GetComponent<CharMotor>();
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hitInfo;
        Vector3 p = new Vector3(tfPlayer.position.x, tfPlayer.position.y, tfPlayer.position.z);
        p.y += 0.2f;
        if (Physics.Raycast(p, -Vector3.up, out hitInfo, Mathf.Infinity))
        {
            if (hitInfo.transform.gameObject.tag == "Hud")
            {
                if (Physics.Raycast(hitInfo.transform.position, -Vector3.up, out hitInfo, Mathf.Infinity))
                {
                    //Vector3 pos = tfPlayer.position;
                    //pos.y -= hitInfo.distance + 0.01f;
                    //tf.position = pos;
                    tf.position = hitInfo.point;
                    tf.rotation = tfPlayer.transform.rotation;
                    //Debug.Log("              "+hitInfo.transform.gameObject.name);
                    cmPlayer.ReceiveCollision(hitInfo.transform.gameObject);
                }
            }
            else
            {
                //Vector3 pos = tfPlayer.position;
                //pos.y -= hitInfo.distance + 0.01f;
                //tf.position = pos;
                tf.position = hitInfo.point;
                tf.rotation = tfPlayer.transform.rotation;
                //Debug.Log(hitInfo.transform.gameObject.name);
                cmPlayer.ReceiveCollision(hitInfo.transform.gameObject);
            }
        }
    }
}
