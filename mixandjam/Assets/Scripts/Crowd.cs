﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Animation_Player;

public class Crowd : MonoBehaviour
{
    //from a jump, at 0.5 it bounces pretty much at always the same 
    private float bounciness = 0.7f;
    private Transform tf;
    private Rigidbody rb;
    private AnimationPlayer ap;
    private Animator animator;

    private bool canJump = true;
    float jumpTimer = 0;
    float moveTimer = 0;
    float rotationTimer = 1;

    
    public float moveDistance = 0.2f;

    Vector3 direction = Vector3.zero;

    void Awake()
    {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        ap = GetComponentInChildren<AnimationPlayer>();
        animator = GetComponentInChildren<Animator>();
       // ap.Play
    }

    // Update is called once per frame
    void Update()
    {
        if (name == "DJ")
            return;
        jumpTimer -= Time.deltaTime;
        moveTimer -= Time.deltaTime;

        if (canJump && jumpTimer<=0)
        {
            rb.AddForce(Vector3.up * 2, ForceMode.Impulse);
            canJump = false;
        }

        if (moveTimer <= 0)
        {
            rotationTimer -= Time.deltaTime;
            
            if (direction != Vector3.zero)
                tf.rotation = Quaternion.Lerp(rb.rotation, Quaternion.LookRotation(direction), Time.deltaTime);
            
            //rb.rotation= Quaternion.LookRotation(direction);

            if (rotationTimer <= 0)
            {
                rb.AddForce(-tf.right * 2, ForceMode.Impulse);
                direction = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1)) * moveDistance * Time.deltaTime;
                moveTimer = Random.Range(0.0f, 5f);
                rotationTimer =1;
            }
        }


        //if (ap && !ap.IsPlaying("Armature|ArmatureAction"))
        //    ap.Play("Armature|ArmatureAction");


    }

    public float GetBounciness()
    {
        return bounciness;
    }

    public void OnCollisionEnter(Collision hit)
    {
        if (hit.gameObject.tag == "Environment")
        {
            canJump = true;

            jumpTimer = Random.Range(0.0f, 1f);
        }
    }
}
