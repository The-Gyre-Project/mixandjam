﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreHud : MonoBehaviour
{

    public Text scoreCount;
    [SerializeField]
    public Text winText;
    [SerializeField]
    public Text winTextAbove;
    [SerializeField]
    public Text winTextBelow;
    private GameObject win;
    public int count = 0;

    // Start is called before the first frame update
    void Start()
    {
        scoreCount = GetComponent<Text>();
        win = GameObject.Find("BGWin");
        win.SetActive(false);
        winTextAbove.text = "";
        winTextBelow.text = "";
        SetCountText();
        winText.text = "";

    }

    private void FixedUpdate()
    {
        SetCountText();
    }

    void SetCountText()
    {
        scoreCount.text = "Score Count: " + count.ToString();
        if (count >= 12)
        {
            //   winText.text = "You Win!";
        }
    }

    public void Win()
    {
        winTextAbove.text = "You Loose!";
        winTextBelow.text = "Press R to restart, press ESC to exit";
        winText.text = "Score: " + count.ToString();
        win.SetActive(true);
    }
}